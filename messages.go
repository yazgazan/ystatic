
package main

const M_config_loaded = "Config loaded"
const M_vars_not_found = "Couldn't load 'vars.json'"
const M_server_start = "Server starting at http://%s/"
const M_server_404 = "File not found."
const M_frame_failed = "Couldn't load '%s'"

